package ru.vartanyan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.vartanyan.tm.configuration.DatabaseConfiguration;
import ru.vartanyan.tm.marker.UnitCategory;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.util.UserUtil;

import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectRepositoryTest {

    @Nullable
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Nullable
    private Project project;

    @NotNull
    private static String USER_ID;

    @Before
    public void before() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext()
                .setAuthentication(authentication);
        USER_ID = UserUtil.getUserId();
        projectRepository.deleteAll();
        @NotNull final Project project = new Project("Project");
        project.setUserId(USER_ID);
        this.project = projectRepository.save(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void add() {
        @Nullable final Project projectById = projectRepository.findById(project.getId()).orElse(null);
        Assert.assertNotNull(projectById);
        Assert.assertEquals(project.getId(), projectById.getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAll() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Project> projects = projectRepository.findAllByUserId(USER_ID);
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Project> projects = projectRepository.findAllByUserId("test");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    @Category(UnitCategory.class)
    public void findById() {
        @Nullable final Project project = projectRepository.findByUserIdAndId(USER_ID, this.project.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Project project = projectRepository.findByUserIdAndId(USER_ID, "34");
        Assert.assertNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdIncorrectUser() {
        @Nullable final Project project = projectRepository.findByUserIdAndId("test", this.project.getId());
        Assert.assertNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void remove() {
        projectRepository.deleteById(project.getId());
        Assert.assertFalse(projectRepository.findById(project.getId()).isPresent());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByName() {
        @NotNull final Project project = projectRepository.findByUserIdAndName(USER_ID, "Project");
        Assert.assertNotNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Project project = projectRepository.findByUserIdAndName(USER_ID, "34");
        Assert.assertNull(project);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Project project = projectRepository.findByUserIdAndName("test", this.project.getName());
        Assert.assertNull(project);
    }

}