package ru.vartanyan.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @WebMethod
    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") @WebParam(name = "id") String id);

    @WebMethod
    @PostMapping("/create")
    Project create(@RequestBody @WebParam(name = "project") Project project);

    @WebMethod
    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody @WebParam(name = "projects") List<Project> projects);

    @WebMethod
    @PostMapping("/save")
    Project save(@RequestBody @WebParam(name = "project") Project project);

    @WebMethod
    @PostMapping("/saveAll")
    List<Project> saveAll(@RequestBody @WebParam(name = "projects") List<Project> projects);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") @WebParam(name = "id") String id);

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}
