package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.vartanyan.tm.api.service.IRoleService;
import ru.vartanyan.tm.exception.entity.UserNotFoundException;
import ru.vartanyan.tm.model.AuthorizedUser;
import ru.vartanyan.tm.model.Role;
import ru.vartanyan.tm.model.User;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Autowired
    private IRoleService roleService;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String userName) throws UsernameNotFoundException {
        @Nullable final User user = findByLogin(userName);
        if (user == null) throw new UserNotFoundException();
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(userName);
        builder.password(user.getPasswordHash());

        @NotNull final List<Role> userRoles = roleService.findAllByUserId(user.getId());
        @NotNull final List<String> roles = new ArrayList<>();
        userRoles.forEach(t -> roles.add(t.toString()));
        builder.roles(roles.toArray(new String[]{}));

        org.springframework.security.core.userdetails.User result = null;
        result = (org.springframework.security.core.userdetails.User) builder.build();

        final AuthorizedUser authorizedUser = new AuthorizedUser(result);
        authorizedUser.setUserId(user.getId());
        return authorizedUser;
    }

    public User findByLogin(@NotNull final String login) {
        return userService.findByLogin(login);
    }

}
