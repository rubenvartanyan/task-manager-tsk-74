package ru.vartanyan.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ApplicationInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(
            @NotNull final SpringApplicationBuilder builder
    ) {
        return builder.sources(Application.class);
    }

}
