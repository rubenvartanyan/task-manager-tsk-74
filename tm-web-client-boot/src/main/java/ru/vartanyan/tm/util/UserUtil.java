package ru.vartanyan.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.vartanyan.tm.exception.system.AccessDeniedException;
import ru.vartanyan.tm.model.AuthorizedUser;
import ru.vartanyan.tm.model.User;
import ru.vartanyan.tm.service.CustomUserDetailsService;

import java.util.Optional;

public class UserUtil implements ApplicationContextAware {

    @NotNull
    private static CustomUserDetailsService userDetailsServiceBean;

    public static String getUserId() {
        @NotNull final Authentication auth = SecurityContextHolder
                .getContext()
                .getAuthentication();
        @Nullable Object principal = auth.getPrincipal();
        Optional.ofNullable(principal).orElseThrow(AccessDeniedException::new);
        if (principal instanceof String) {
            @Nullable final User user = userDetailsServiceBean
                    .findByLogin(principal.toString());
            Optional.ofNullable(user).orElseThrow(AccessDeniedException::new);
            return user.getId();
        }
        if (principal instanceof AuthorizedUser) {
            @NotNull AuthorizedUser authorizedUser = (AuthorizedUser) principal;
            return authorizedUser.getUserId();
        }
        throw new AccessDeniedException();
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        userDetailsServiceBean = (CustomUserDetailsService)
                applicationContext.getBean("userDetailsService");
    }
}
