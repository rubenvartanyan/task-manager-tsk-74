package ru.vartanyan.tm.exception;

import org.jetbrains.annotations.NotNull;

public abstract class AbstractException extends RuntimeException {

    @NotNull
    public AbstractException() {
        super();
    }

    @NotNull
    public AbstractException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    @NotNull
    public AbstractException(@NotNull Throwable cause) {
        super(cause);
    }

    @NotNull
    public AbstractException(@NotNull String s) {
        super(s);
    }

}
