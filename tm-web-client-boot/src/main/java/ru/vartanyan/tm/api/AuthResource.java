package ru.vartanyan.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.vartanyan.tm.dto.Result;

@RequestMapping("/auth")
public interface AuthResource {

    @GetMapping("/login")
    public Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    );

    @GetMapping("/logout")
    public Result logout();

}
