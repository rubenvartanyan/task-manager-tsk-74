package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService extends IRecordService<Task> {

    Task findByName(String userId, String name);

    void removeByName(String userId, String name);

    Task updateById(String userId, final String id, final String name, final String description);

    Task startById(String userId, String id);

    Task startByName(String userId, String name);

    Task finishById(String userId, String id);

    Task finishByName(String userId, String name);

    Task add(String user, String name, String description);

    List<Task> findAll(@NotNull String userId);

    void addAll(String userId, @Nullable Collection<Task> collection);

    Task add(String user, @Nullable Task entity);

    Task findById(@NotNull String userId, @Nullable String id);

    void clear(@NotNull String userId);

    void removeById(@NotNull String userId, @Nullable String id);

    void remove(@NotNull String userId, @Nullable Task entity);

}
