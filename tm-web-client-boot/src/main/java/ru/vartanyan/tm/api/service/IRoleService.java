package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.model.Role;

import java.util.List;

public interface IRoleService extends IRecordService<Role> {

    @NotNull
    List<Role> findAllByUserId(@NotNull String userId);

    void clear(@NotNull String userId);

}
