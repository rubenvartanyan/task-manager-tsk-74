package ru.vartanyan.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.endpoint.EndpointLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.configuration.ClientConfiguration;
import ru.vartanyan.tm.marker.IntegrationCategory;

import java.util.List;

public class AdminEndpointTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    private final AdminEndpoint adminEndpoint = context.getBean(AdminEndpoint.class);
    
    private final UserEndpoint userEndpoint = context.getBean(UserEndpoint.class);

    private SessionDTO sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLoginTest() {
        UserDTO user = new UserDTO();
        user.setLogin("AAA");
        adminEndpoint.addUser(user, sessionAdmin);
        adminEndpoint.removeUserByLogin("AAA", sessionAdmin);
        Assert.assertNull(adminEndpoint.findUserByItsLogin("AAA", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByIdTest() {
        adminEndpoint.createUser("XXX", "XXX", sessionAdmin);
        final UserDTO user = adminEndpoint.findUserByItsLogin("XXX", sessionAdmin);
        final String userId = user.getId();
        final UserDTO userFound = adminEndpoint.findUserById(userId, sessionAdmin);
        Assert.assertEquals("XXX", userFound.getLogin());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findUserByLogin() {
        adminEndpoint.createUser("MMM", "MMM", sessionAdmin);
        final UserDTO userFound = adminEndpoint.findUserByItsLogin("MMM", sessionAdmin);
        Assert.assertNotNull(userFound);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithEmailTest() {
        adminEndpoint.createUserWithEmail("test1", "test1", "test@tsk.ru", sessionAdmin);
        Assert.assertNotNull(userEndpoint.findUserByLogin("test1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserTest() {
        adminEndpoint.createUser("login1", "password1", sessionAdmin);
        Assert.assertNotNull(userEndpoint.findUserByLogin("test1", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void createUserWithRoleTest() {
        adminEndpoint.createUserWithRole("QQQQ", "password1", Role.USER, sessionAdmin);
        Assert.assertNotNull(userEndpoint.findUserByLogin("QQQQ", sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByLoginTest() {
        adminEndpoint.createUser("EEE", "EEE", sessionAdmin);
        UserDTO user = adminEndpoint.findUserByItsLogin("EEE", sessionAdmin);
        adminEndpoint.lockUserByLogin("EEE", sessionAdmin);
        Assert.assertTrue(user.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void lockUserByIdTest() {
        adminEndpoint.createUser("RRR", "RRR", sessionAdmin);
        UserDTO user = adminEndpoint.findUserByItsLogin("RRR", sessionAdmin);
        adminEndpoint.lockUserById(user.id, sessionAdmin);
        Assert.assertTrue(user.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByLoginTest() {
        adminEndpoint.createUser("FFF", "FFF", sessionAdmin);
        UserDTO user = adminEndpoint.findUserByItsLogin("FFF", sessionAdmin);
        adminEndpoint.lockUserByLogin("FFF", sessionAdmin);
        adminEndpoint.unlockUserByLogin("FFF", sessionAdmin);
        Assert.assertFalse(user.locked);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void unlockUserByIdTest() {
        adminEndpoint.createUser("KKK", "KKK", sessionAdmin);
        final UserDTO user = adminEndpoint.findUserByItsLogin("KKK", sessionAdmin);
        final String userId = user.getId();
        adminEndpoint.lockUserById(userId, sessionAdmin);
        adminEndpoint.unlockUserById(userId, sessionAdmin);
        Assert.assertFalse(user.locked);
    }


    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByIdTest() {
        adminEndpoint.createUser("ZZZ", "SSS", sessionAdmin);
        final UserDTO user = adminEndpoint.findUserByItsLogin("ZZZ", sessionAdmin);
        final String userId = user.getId();
        adminEndpoint.removeUserById(userId, sessionAdmin);
        Assert.assertNull(adminEndpoint.findUserById(userId, sessionAdmin));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeUserByLoginTest() {
        adminEndpoint.createUser("DDD", "DDD", sessionAdmin);
        final UserDTO user = adminEndpoint.findUserByItsLogin("DDD", sessionAdmin);
        adminEndpoint.removeUserByLogin("DDD", sessionAdmin);
        Assert.assertNull(adminEndpoint.findUserByItsLogin("DDD", sessionAdmin));
    }

}
