package ru.vartanyan.tm.exception.incorrect;

public class IncorrectIndexException extends Exception{

    public IncorrectIndexException(final int index) throws Exception {
        super("Error! " + index + " is incorrect index...");
    }

}
