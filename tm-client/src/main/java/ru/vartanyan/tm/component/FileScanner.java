package ru.vartanyan.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.event.ConsoleEvent;
import ru.vartanyan.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class FileScanner implements Runnable {

    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    private static final int INTERVAL = 3;

    private static final String PATH = "./";

    private final Bootstrap bootstrap;

    private final Collection<String> commands = new ArrayList<>();

    public FileScanner(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        for (@NotNull final AbstractListener command : listeners) {
            if (command.arg() != null) commands.add(command.name());
        }
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void run() {
        final File file = new File(PATH);
        for (final File item: file.listFiles()){
            if (!item.isFile()) continue;
            final String fileName = item.getName();
            final boolean check = commands.contains(fileName);
            if (!check) continue;
            publisher.publishEvent(new ConsoleEvent(fileName));
            item.delete();
        }
    }

}
