package ru.vartanyan.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.AbstractEntity;

import java.util.List;

public interface IService<E extends AbstractEntity> {

    void add(@Nullable E entity);

    void addAll(@Nullable List<E> entities);

    void deleteAll();

    @Nullable
    List<E> findAll();

    @Nullable
    E findOneById(@Nullable String id);

    void removeOneById(@Nullable String id);

}

