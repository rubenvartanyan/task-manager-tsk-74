package ru.vartanyan.tm.api.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.model.SessionGraph;

public interface ISessionServiceGraph extends IServiceGraph<SessionGraph> {

    @Nullable SessionGraph close(@Nullable SessionGraph session);

    @Nullable
    SessionGraph open(String login,
                      String password);

    void validate(@Nullable SessionGraph session);

    void validateAdmin(@Nullable SessionGraph session,
                       @Nullable Role role);

    @SneakyThrows
    void remove(@Nullable SessionGraph entity);

}
