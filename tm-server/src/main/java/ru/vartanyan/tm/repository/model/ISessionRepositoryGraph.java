package ru.vartanyan.tm.repository.model;


import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.vartanyan.tm.api.repository.IRepositoryGraph;
import ru.vartanyan.tm.dto.Session;
import ru.vartanyan.tm.model.SessionGraph;

import javax.persistence.EntityManager;
import java.util.List;

public interface ISessionRepositoryGraph extends IRepositoryGraph<SessionGraph> {

    @Nullable
    SessionGraph findOneById(@Nullable final String id);

}